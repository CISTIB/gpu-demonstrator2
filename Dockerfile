#FROM nvcr.io/nvidia/tensorflow:20.01-tf1-py3
FROM centos:7

# install packages
RUN yum update -y && yum upgrade -y && yum install -y git wget && yum group install "Development Tools" -y

#Compiling and installing MPI 3.1.4
RUN wget https://download.open-mpi.org/release/open-mpi/v3.1/openmpi-3.1.4.tar.bz2
RUN tar xvjf openmpi-3.1.4.tar.bz2 && cd openmpi-3.1.4 && bash ./configure --prefix=/usr/local --enable-mpi-cxx --with-cma --with-sge --enable-mpi-thread-multiple  &&  make all install


# Enable systemd 
ENV container docker
RUN (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == \
systemd-tmpfiles-setup.service ] || rm -f $i; done); \
rm -f /lib/systemd/system/multi-user.target.wants/*;\
rm -f /etc/systemd/system/*.wants/*;\
rm -f /lib/systemd/system/local-fs.target.wants/*; \
rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
rm -f /lib/systemd/system/basic.target.wants/*;\
rm -f /lib/systemd/system/anaconda.target.wants/*;
VOLUME [ "/sys/fs/cgroup" ]
CMD ["/usr/sbin/init"]

	
# clone code repository
RUN cd / && git clone https://KHozdevila@bitbucket.org/CISTIB/gpu-demonstrator2.git


# Setting the environment variables for HGDL
ENV LD_LIBRARY_PATH=/gpu-demonstrator2/LinuxHGDL/:$LD_LIBRARY_PATH
ENV PATH=/gpu-demonstrator2/LinuxHGDL/:$PATH
ENV BASH_ENV=/root/.bashenv
ENV MPI_LIB=/usr/lib64/openmpi/lib
ENV MPI_COMPILER=openmpi-x86_64
ENV MPI_INCLUDE=/usr/include/openmpi-x86_64
ENV MPI_BIN=/usr/lib64/openmpi/bin
ENV MPI_SYSCONFIG=/etc/openmpi-x86_64
ENV MPI_SUFFIX=_openmpi
ENV MPI_MAN=/usr/share/man/openmpi-x86_64
ENV MPI_HOME=/usr/lib64/openmpi
ENV MPI_FORTRAN_MOD_DIR=/usr/lib64/gfortran/modules/openmpi-x86_64

# Setting permissions in the execution folder
RUN chmod +x /gpu-demonstrator2/LinuxHGDL/HGDL2 
RUN chmod +x /gpu-demonstrator2/LinuxHGDL/hgdl_test.sh 

#Loading mpi module
#RUN module load mpi/openmpi-x86_64

#Adding MPI help files
RUN ln -s /gpu-demonstrator2/apps /apps

RUN cd /gpu-demonstrator2/LinuxHGDL/TestData

WORKDIR /gpu-demonstrator2/LinuxHGDL/TestData

# Execute HGDL2
#RUN /gpu-demonstrator2/LinuxHGDL/HGDL2


